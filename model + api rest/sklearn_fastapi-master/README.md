### API

- Pour lancer l'application en local, 2 étapes sont nécessaires :
    1. installation de l'environnement python. Pour cela il faut se placer à la racine du projet et
    installer tous les requirements via la commande suivante : pip install -r requirements.txt.
    2. lancement de l'application : uvicorn app.main:app
    La documentation openapi est générée automatiquement et est accessible à l'adresse suivante : localhost:8000/docs
