import pickle
import pandas as pd
from scipy.spatial import distance


class Model:

    def __init__(self):
        # self.model = pickle.load(open(path, 'rb'))
        self.tup = None
        self.s = None
        self.ar = None
        self.b = None
        self.data = pd.read_csv('../../data/spotify/data.csv')
        self.data.drop(columns=['id', 'release_date'], inplace=True)

    def predict(self, ans, b):
        return self.make_matrix_correlation(self.s[int(ans)], b, self.ar[int(ans)])

    def find_similar_song(self, song):
        self.tup, self.s, self.ar = self.find_word(song)
        return self.tup, self.s, self.ar

    def find_word(self, word, number=10):
        self.b = number
        data = self.data.drop_duplicates()
        words = data['name'].values
        artists = data['artists'].values
        t = []
        count = 0
        if word[-1] == ' ':
            word = word[:-1]
        for i in words:
            if word.lower() in i.lower():
                t.append([len(word) / len(i), count])
            else:
                t.append([0, count])
            count += 1
        t.sort(reverse=True)
        s = [[words[t[i][1]], artists[t[i][1]].strip('][').split(', ')] for i in range(number)]
        songs = [words[t[i][1]] for i in range(number)]
        artist = [artists[t[i][1]] for i in range(number)]
        x = []
        for i in s:
            l = ''
            by = ''
            for j in i[1]:
                by += j
            l += i[0] + ' by ' + by
            x.append(l)
        tup = []
        for i in range(number):
            tup.append((x[i], i))

        return tup, songs, artist

    def make_matrix_correlation(self, best, number, artist):
        data = self.data.drop_duplicates(subset=['artists', 'name'])
        x = data[(data['name'] == best) & (data['artists'] == artist)].drop(
            columns=['name', 'artists']).values
        print(x)
        artist = artist.replace("'", "").replace("'", "").replace('[', '').replace(']', '')
        if ',' in artist:
            inm = artist.rfind(",")
            artist = artist[:inm] + ' and' + artist[inm + 1:]
        print('The song closest to your search is :', best, ' by ', artist)

        song_names = data['name'].values
        #    df=df.fillna(df.mean())
        p = []
        count = 0
        for i in data.drop(columns=['artists', 'name']).values:
            p.append([distance.correlation(x, i), count])
            count += 1
        p.sort()
        res = []
        for i in range(1, number + 1):
            artists = data['artists'].values
            artist = artists[p[i][1]]
            artist = artist.replace("'", "").replace("'", "").replace('[', '').replace(']', '')
            if ',' in artist:
                inm = artist.rfind(",")
                artist = artist[:inm] + ' and' + artist[inm + 1:]
            print(song_names[p[i][1]] + ' by ' + artist)
            res.append({"title": song_names[p[i][1]], "artist": artist})

        return res
