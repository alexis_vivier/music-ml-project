from typing import Callable
from fastapi import FastAPI
# from loguru import logger
import pickle
from app.services.model import Model

from app.core.config import DEFAULT_MODEL_PATH


def _shutdown_model(app: FastAPI) -> None:
    app.state.model = None
    pass


def stop_app_handler(app: FastAPI) -> Callable:
    def shutdown() -> None:
        # logger.info("Running app shutdown handler.")
        _shutdown_model(app)

    return shutdown


def start_app_handler(app: FastAPI) -> Callable:
    def startup() -> None:
        # logger.info("Running app start handler.")
        _startup_model(app)

    return startup


def _startup_model(app: FastAPI) -> None:
    model_path = DEFAULT_MODEL_PATH
    model_instance = Model()  # use joblib or pickle.load, etc.
    app.state.model = model_instance
    pass


def _shutdown_model(app: FastAPI) -> None:
    app.state.model = None
    pass
