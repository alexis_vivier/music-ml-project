from fastapi import APIRouter
import numpy as np
from app.models.features import Features
from starlette.requests import Request
import codecs, json
from app.services.model import Model

router = APIRouter()
model = Model()


@router.get("/")
def read_root():
    return 'Hello World'


@router.post("/predict/", name="predict")
async def predict(
        ans: int,
        number: int,
        # features: Test,
        request: Request
):
    return json.dumps({"response": request.app.state.model.predict(ans, number)})


@router.post("/find_similar_song/", name="find_similar_song")
async def find_similar_song(
        song: str,
        request: Request
):
    tup, s, ar = request.app.state.model.find_similar_song(song)
    return json.dumps({
        "options": tup,
        "description": 'Closest songs to :' + song,
    })
