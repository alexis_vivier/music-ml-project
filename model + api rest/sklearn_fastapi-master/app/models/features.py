from pydantic import BaseModel
from typing import List


class TextSample(BaseModel):
    text: str


class Features(BaseModel):
    samples: List[TextSample]

    def to_array(self):
        return [sample.text for sample in self.samples]
