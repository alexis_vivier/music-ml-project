# Projet fil rouge

## Présentation

Le projet a été présenté par la société **Your Next Favorite Song**. Nous sommes une équipe composée de 4 personnes travaillant dans l'intelligence artificielle et plus précisément dans le machine learning.

## Ressources

Cette partie traite des ressources que nous avons utilisé pendant le projet.

### Datasets

- https://archive.org/details/thisismyjam-datadump
- https://www.kaggle.com/yamaerenayspotify-dataset-19212020-160k-tracks

### Notebooks externes

- https://www.kaggle.com/anatpeled/spotify-popularity-prediction


## Déroulement: 

Nous avons travailler sur plusieurs points :
- Préparer les données pour les utiliser dans le projet.
- Creation de la data visualisation pour présenter au client et expliquer notre demarche (disponible au format PDF)
- Recherche d'un modéle de prédiction
- création d'une interface
- Mise en place du modèle avec l'interface


## Solution 2 retenue:

- Une Interface simple : un champ guidé pour une musique
- Permettre de décharger les experts dans un premier temps
- Calcul des distances avec la musique en entrée pour toutes les features
- utilisation du jeux de donnée de Spotify 


## Amélioration: 

- Utiliser un modèle k-NN
- Ajout d’un système de recommandation par utilisateur
- Ajouter plus de fonctionnalités sur l’interface web


##Vous trouverez dans les dossiers respectifs le PDF avec l'ensemble de la Data Visualisation, le NoteBook fonctionnelle, L'API a lancer et pour terminer le PowerPoint dans le dossier ppt.
